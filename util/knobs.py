import json
import numpy as np
import bisect
import math

KNOB_DETAILS = None


def gen_continuous(action):
    knobs = {}

    for idx in range(len(KNOBS)):
        name = KNOBS[idx]
        value = KNOB_DETAILS[name]

        knob_type = value['type']

        if knob_type == 'integer':
            min_val, max_val = value['min'], value['max']
            delta = int((max_val - min_val) * action[idx])
            eval_value = min_val + delta 
            eval_value = max(eval_value, min_val)
            if value.get('stride'):
                all_vals = np.arange(min_val, max_val, value['stride'])
                indx = bisect.bisect_left(all_vals, eval_value)
                if indx == len(all_vals): indx -= 1
                eval_value = all_vals[indx]

            knobs[name] = eval_value
        if knob_type == 'float':
            min_val, max_val = value['min'], value['max']
            delta = (max_val - min_val) * action[idx]
            eval_value = min_val + delta
            eval_value = max(eval_value, min_val)
            knobs[name] = eval_value
        elif knob_type == 'enum':
            enum_size = len(value['enum_values'])
            enum_index = int(enum_size * action[idx])
            enum_index = min(enum_size - 1, enum_index)
            eval_value = value['enum_values'][enum_index]
            knobs[name] = eval_value
        elif knob_type == 'combination':
            enum_size = len(value['combination_values'])
            enum_index = int(enum_size * action[idx])
            enum_index = min(enum_size - 1, enum_index)
            eval_value = value['combination_values'][enum_index]
            knobs_names = name.strip().split('|')
            knobs_value = eval_value.strip().split('|')
            for k, knob_name_tmp in enumerate(knobs_names):
                knobs[knob_name_tmp] = knobs_value[k]


    return knobs

def initialize_knobs(knobs_config, num):
    global KNOBS
    global KNOB_DETAILS
    if num == -1:
        f = open(knobs_config)
        KNOB_DETAILS = json.load(f)
        KNOBS = list(KNOB_DETAILS.keys())
        f.close()
    else:
        f = open(knobs_config)
        knob_tmp = json.load(f)
        i = 0
        KNOB_DETAILS = {}
        while i < num:
            key = list(knob_tmp.keys())[i]
            KNOB_DETAILS[key] = knob_tmp[key]
            i = i + 1
        KNOBS = list(KNOB_DETAILS.keys())
        f.close()
    return KNOB_DETAILS


def get_default_knobs():
    default_knobs = {}
    for name, value in KNOB_DETAILS.items():
        if not value['type'] == "combination":
            default_knobs[name] = value['default']
        else:
            knobL = name.strip().split('|')
            valueL = value['default'].strip().split('|')
            for i in range(0, len(knobL)):
                default_knobs[knobL[i]] = int(valueL[i])
    return default_knobs


def get_knob_details(knobs_config, num = -1):
    initialize_knobs(knobs_config, num)
    return KNOB_DETAILS

def knob2action(knob):
    actionL = []
    for idx in range(len(KNOBS)):
        name = KNOBS[idx]
        value = KNOB_DETAILS[name]
        knob_type = value['type']
        if knob_type == "enum":
            enum_size = len(value['enum_values'])
            action = value['enum_values'].index(knob[name]) / enum_size
        else:
            min_val, max_val = value['min'], value['max']
            action = (knob[name] - min_val) / (max_val - min_val)

        actionL.append(action)

    return np.array(actionL)

def knob2domain(knob):
    domain = []
    for key in knob:
        d = {}
        d['name'] = key
        # if knob[key]['type'] == 'enum':
        #     d['type'] = 'discrete'
        #     d['domain'] = tuple(knob[key]['enumvals'])
        # else:
        #     d['type'] = 'continuous'
        #     d['domain'] = (knob[key]['min'], knob[key]['max'])
        d['type'] = 'continuous'
        d['domain'] = (0, 1)
        domain.append(d)

    return domain

def get_context_domain(context_dim):
    domain = []
    for i in range(context_dim):
        d = {}
        d['name'] = 'context%s'%i
        d['type'] = 'continuous'
        if i == context_dim -1:
            d['domain'] = (0, 1000)
        else:
            d['domain'] = (-10, 10)
        domain.append(d)

    return domain

def get_knobs_radius(knobs_detail):
    radius = 0 
    for knob in knobs_detail:
        # radius += (knobs_detail[knob]['max'] - knobs_detail[knob]['min']) ** 2
        radius += 1

    return math.sqrt(radius)
