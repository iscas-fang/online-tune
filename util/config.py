import json
import sys
import ConfigSpace
import numpy as np
import pandas as pd
from typing import List
from collections import defaultdict

from ConfigSpace import ConfigurationSpace, UniformIntegerHyperparameter, UniformFloatHyperparameter, \
    CategoricalHyperparameter, Constant, Configuration
from ConfigSpace import EqualsCondition, InCondition
from ConfigSpace import ForbiddenEqualsClause, ForbiddenAndConjunction, ForbiddenInClause
from ConfigSpace.util import deactivate_inactive_hyperparameters
from .knobs import get_knob_details

def setup_configuration_space(knob_file, num = -1):
    config_space = ConfigurationSpace()
    knobs_detail = get_knob_details(knob_file, num)
    KNOBS = knobs_detail
    knobs_list = []

    for name in KNOBS.keys():
        value = KNOBS[name]
        knob_type = value['type']
        if knob_type == 'enum':
            min_val, max_val = value['min'], value['max']
            knob = UniformIntegerHyperparameter(name, min_val, max_val, default_value=value['default'])
            #knob = CategoricalHyperparameter(name, [str(i) for i in value["enumvals"]], default_value= str(value['default']))
        elif knob_type == 'integer':
            min_val, max_val = value['min'], value['max']
            if knobs_detail[name]['max'] > sys.maxsize:
                knob = UniformIntegerHyperparameter(name, int(min_val / 1000), int(max_val / 1000),
                                                    default_value=int(value['default'] / 1000))
            else:
                knob = UniformIntegerHyperparameter(name, min_val, max_val, default_value=value['default'])
        elif knob_type == 'float':
            min_val, max_val = value['min'], value['max']
            knob = UniformFloatHyperparameter(name, min_val, max_val, default_value=value['default'])
        else:
            raise ValueError('Invalid knob type!')

        knobs_list.append(knob)

    config_space.add_hyperparameters(knobs_list)
    return config_space

def config2df(configs):
    config_dic = defaultdict(list)
    for config in configs:
        for k in config:
            config_dic[k].append(config[k])

    return pd.DataFrame.from_dict(config_dic)


def convert_configurations_to_array(configs: List[Configuration]) -> np.ndarray:
    """Impute inactive hyperparameters in configurations with their default.

    Necessary to apply an EPM to the data.

    Parameters
    ----------
    configs : List[Configuration]
        List of configuration objects.

    Returns
    -------
    np.ndarray
        Array with configuration hyperparameters. Inactive values are imputed
        with their default value.
    """
    configs_array = np.array([config.get_array() for config in configs],
                             dtype=np.float64)
    configuration_space = configs[0].configuration_space
    return impute_default_values(configuration_space, configs_array)


def impute_default_values(
        configuration_space: ConfigurationSpace,
        configs_array: np.ndarray
) -> np.ndarray:
    """Impute inactive hyperparameters in configuration array with their default.

    Necessary to apply an EPM to the data.

    Parameters
    ----------
    configuration_space : ConfigurationSpace
    
    configs_array : np.ndarray
        Array of configurations.

    Returns
    -------
    np.ndarray
        Array with configuration hyperparameters. Inactive values are imputed
        with their default value.
    """
    for hp in configuration_space.get_hyperparameters():
        default = hp.normalized_default_value
        idx = configuration_space.get_idx_by_hyperparameter_name(hp.name)
        nonfinite_mask = ~np.isfinite(configs_array[:, idx])
        configs_array[nonfinite_mask, idx] = default

    return configs_array