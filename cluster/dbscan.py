import os
import numpy as np
from sklearn.cluster import DBSCAN
from sklearn import metrics
from sklearn.svm import SVC
import pandas as pd
import math
from sklearn import preprocessing
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from util.knobs import knob2action, initialize_knobs, gen_continuous, get_default_knobs
import time

class Cluster():
    def __init__(self, knob_len = 10, context_len =10, algo_kwargs=None, dynamic=True):
        
        self.svm = SVC(kernel="rbf", gamma="auto")
    
        self.append_count = 0
        self.current_log = 0
        self.no_cluster = True
        self.context_len = context_len
        self.knob_len = knob_len
        self.init_empty_cluster()

    def init_empty_cluster(self):
        self.contextL = np.empty((0, self.context_len))
        self.tpmL = np.empty(0)
        self.knobL = np.empty((0, self.knob_len))
        self.dbscan_label = np.empty(0)
        print("init empty cluster")


    def dbscan_(self, X, replace=True):
        # scaler = StandardScaler()
        # X = scaler.fit_transform(X)
        # pca = PCA(n_components=5)
        # pca.fit(X)
        # X = pca.transform(X)
        db = DBSCAN(eps=0.16, min_samples = 4).fit(X)
        core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
        core_samples_mask[db.core_sample_indices_] = True
        labels = db.labels_
        # Number of clusters in labels, ignoring noise if present.
        n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
        n_noise_ = list(labels).count(-1)

        if n_clusters_ == 0 :
            silhouette_score = 0
        elif n_clusters_ == 1 and n_noise_ == 0:
            silhouette_score = 1
        else:
            silhouette_score = metrics.silhouette_score(X, labels)


        if replace:
            if n_clusters_ == 0:
                self.no_cluster = True
            else:
                self.no_cluster = False

            #print(labels)
            print('Estimated number of clusters: %d' % n_clusters_)
            print('Estimated number of noise points: %d' % n_noise_)
            print("Silhouette Coefficient: %0.3f" % silhouette_score)

            self.dbscan_label = labels
            self.silhouette_score = silhouette_score
            self.learn_boundary()
            return labels
        else:
            return labels, silhouette_score


    def clusters_to_files(self, labelL):
        lines = np.array(self.lines_filter)
        label_set = list(set(labelL))

        if not os.path.exists('cluster_res'):
            os.system('mkdir cluster_res')

        for label in label_set:
            idx = np.where(np.array(labelL) == label)[0]
            lines_filter = lines[idx]
            lines_filter = list(lines_filter)
            f_out = os.path.join('cluster_res', 'label{}.txt'.format(label))
            f = open(f_out, 'w')
            f.writelines(lines_filter)
            f.flush()
            f.close()

    def clusters_to_gps_objective_one_label(self, label, dynamic=True):
        idx_label = np.where(np.array(self.dbscan_label) == label)[0]
        idx = np.array(list(set(idx_label)))
        X_0 = self.knobL[idx]
        tpm = self.tpmL[idx]
        context = self.contextL[idx]
        # if dynamic:
        #     X = np.hstack((X_0, context))
        # else:
        X = X_0.copy()
        CX = context.copy()
        Y = np.vstack(tpm) 

        return X , CX, Y


    def clusters_to_gps_objective(self, labelL):
        label_set = list(set(labelL))
        if not os.path.exists('cluster_gp_objective'):
            os.system('mkdir cluster_gp_objective')

        for label in label_set:
            self.clusters_to_gps_objective_one_label(label)



    def learn_boundary(self):
        labels = self.dbscan_label
        n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
        if n_clusters_ == 0:
            self.no_cluster = True
        else:
            self.svm.fit(self.contextL, self.dbscan_label)
            self.no_cluster = False


    def predict_cluster(self, x):
        if self.no_cluster:
            label = -1
        else:
            label = self.svm.predict(x.reshape(-1, self.contextL[0].shape[0]))[0]
        print("cluster {} is chosen".format(label))
        return label


    def get_label(self, id):
        for label in self.label_id:
            if self.label_id[label] == id:
                return label

    def ifReCluster(self):
        if self.append_count < 10:
            return False
        label_cluster, silhouette_score_cluster = self.dbscan_(self.contextL, replace=False)

        if self.no_cluster or len(set(label_cluster)) == 1:
            silhouette_score_no_cluster = 0
        else:
            # scaler = StandardScaler()
            # X = scaler.fit_transform(self.contextL)
            # pca = PCA(n_components=5)
            # pca.fit(X)
            # X = pca.transform(X)
            X = self.contextL
            silhouette_score_no_cluster = metrics.silhouette_score(X, self.dbscan_label)
        adjusted_mutual_info = metrics.adjusted_mutual_info_score(label_cluster, self.dbscan_label)
        print("Adjusted Mutual Information: {}, silhouette_score_no_cluster: {}, silhouette_score_cluster: {}".format(adjusted_mutual_info, silhouette_score_no_cluster, silhouette_score_cluster))
        # print("no cluster labels{}".format(self.dbscan_label))
        # print("cluster labels{}".format(label_cluster))

        if  len(set(label_cluster)) > len(set(self.dbscan_label)):
            return True

        if silhouette_score_no_cluster  < silhouette_score_cluster and adjusted_mutual_info < 0.7:
            return True


    def add_data_to_cluster(self, context, tpm, knobs, label):
        self.contextL = np.vstack((self.contextL, context))
        self.knobL = np.vstack((self.knobL, knobs))
        self.tpmL = np.hstack((self.tpmL, tpm))
        self.dbscan_label = np.hstack((self.dbscan_label, label))
        self.append_count = self.append_count + 1