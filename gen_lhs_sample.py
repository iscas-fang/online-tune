import json
import sys
import ConfigSpace
import numpy as np

from sampler.samplers import SobolSampler, LatinHypercubeSampler
from util.config import setup_configuration_space


if __name__ == '__main__':
    config_space = setup_configuration_space('sampler/dmdb8_sys_knobs.json')
    default_config = config_space.get_default_configuration()
    
    lhs = LatinHypercubeSampler(config_space, 300, criterion='maximin')
    initial_configs = [default_config] + lhs.generate(return_config=True)

    sampler = np.array(initial_configs)
    np.save('data/samples_data/samples.npy', sampler)

    

    
