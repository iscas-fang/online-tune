import os

#==========================================================
#  HOST LOGIN
#==========================================================

# Location of the database host relative to this driver
# Valid values: local, remote
HOST_CONN = 'local'

# Host SSH login credentials (only required if HOST_CONN=remote)
LOGIN_NAME = ''
LOGIN_HOST = ''
LOGIN_PASSWORD = ''
LOGIN_PORT = None  # Set when using a port other than the SSH default

#==========================================================
#  DATABASE OPTIONS
#==========================================================

DB_TYPE = ''

# Database username
DB_USER = ''

# Password for DB_USER
DB_PASSWORD = ''

# Database host address
DB_HOST = ''

# Database port
DB_PORT = ''

# Base config settings to always include when installing new configurations
if DB_TYPE == 'dmdb':
    BASE_DB_CONF = {
        'SORT_FLAG': 3,
        'SORT_ADAPTIVE_FLAG': 3,
        'MMT_SIZE' : 32,
    }
else:
    BASE_DB_CONF = None

# Name of the device on the database server to monitor the disk usage, or None to disable
DATABASE_DISK = None

# DMDB-SPECFIC OPTIONS >>>
DMDB_ROOT_PATH = ''
DMDB_INSTANCE_PATH = ''
DMDB_DUMP_DIR = ''
DMDB_SQLLOG_PATH = ''
DMDB_SQLLOG_FIIE = ''

#DMDB-KNOBS OPTIONS
DYNAMIC_KNOBS_FILE = os.path.expanduser('~/online-tune/sampler/dmdb8_knobs_shap.json')
KNOBS_NUM = 10
CONTEXT_DIM = 13

# The maximum allowable disk usage percentage. Reload the database
# whenever the current disk usage exceeds this value.
MAX_DISK_USAGE = 90

#==========================================================
#  OLTPBENCHMARK OPTIONS
#==========================================================

# Path to OLTPBench directory
OLTPBENCH_HOME = os.path.expanduser('~/benchbase-dmdb')

# Path to the OLTPBench configuration file
OLTPBENCH_CONFIG = os.path.join(OLTPBENCH_HOME, 'config/dmdb/sample_')

RUN_TIME = 240

#==========================================================
#  LOGGING OPTIONS
#==========================================================

LOG_LEVEL = 'DEBUG'

# Path to log directory
LOG_DIR = os.path.expanduser('~/online-tune/log')

DRIVER_LOG = os.path.join(LOG_DIR, 'driver.log')
# Log files
OLTPBENCH_LOG = os.path.join(LOG_DIR, 'oltpbench.log')
BENCHMARKSQL_LOG = os.path.join(LOG_DIR, 'benchmarksql.log')



#==========================================================
#  TUNING OPTIONS
#==========================================================
WORKLOAD2VEC_PATH = os.path.expanduser("~/online-tune/workload2vec")

# 'online-tune'
TUNE_METHORD = "online-tune"

#'throughput' , 'avg_latency'
TUNE_TARGET = 'throughput'

#workload 
WORKLOAD_PATTEN = ["tpcc15", "tpcc45", "tpcc75"]

#epoch
EPOCH = 2

#iteration
ITER = 2

