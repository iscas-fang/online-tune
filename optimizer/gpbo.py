import GPy
import GPyOpt
from GPyOpt.methods import BayesianOptimization
from GPyOpt.core.task.space import Design_space
from GPyOpt.experiment_design import initial_design

import numpy as np

class GPBO():
    def __init__(self, func, domain_all, knob_dim, history=None, initial_num = 1):
        
        self.domain_all = domain_all
        self.func = func
        self.knob_dim = knob_dim
        self.gp_kernel = self._get_kernel()
        self.history = history
        self.objective_num = 0
        self.initial_num = initial_num
        self._get_initial()
        self._init_bo(self.domain_all, self.knobL, self.tpmL)
        # self._initial()

    def _init_bo(self, domain , X, Y, constraints = None):
        self.bo = BayesianOptimization(f= self.func, initial_design_numdata=0,
                                       domain= domain,
                                       constraints = constraints,
                                       kernel=self.gp_kernel,
                                       X=X,
                                       Y=Y,
                                       acquisition_type='LCB',
                                       num_cores = 4,
                                       Acquisition_optimizer_type = 'CMA')

    def _get_kernel(self):
        return  GPy.kern.sde_Matern52(input_dim=self.knob_dim , variance=2., lengthscale=0.01, active_dims=[i for i in range(0, self.knob_dim)], ARD=True)

    def _get_initial(self):
        #todo 
        self.knobL = np.empty(shape = (0, self.knob_dim))
        self.tpmL = np.empty(shape = (0, 1))
    
        space = Design_space(self.domain_all)
        self.initial_data = initial_design('latin', space, self.initial_num)
     
    def add_single_data(self, x, y):

        self.knobL = np.vstack([self.knobL, x])
        self.tpmL = np.vstack([self.tpmL, y])

        self.bo.X = np.vstack([self.bo.X, x])
        self.bo.Y = np.vstack([self.bo.Y, y])
        self.objective_num += 1


    def suggets(self):

        if len(self.tpmL) < self.initial_num:
            print("suggest random config!")
            return self.initial_data[self.objective_num]

        next_point = self.bo.suggest_next_locations()
        
        new_point = []
        for i in range(self.knob_dim):
            new_point.append(next_point[0][i])

        return new_point