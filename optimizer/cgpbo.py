import GPy
import GPyOpt
from GPyOpt.methods import BayesianOptimization
from GPyOpt.core.task.space import Design_space
from GPyOpt.experiment_design import initial_design

import numpy as np

class CGPBO():
    def __init__(self, func, domain_all, knob_dim, max_radius ,context_dim = 0, history=None, initial_num = 1):
        
        self.domain_all = domain_all
        self.func = func
        self.knob_dim = knob_dim
        self.context_dim = context_dim
        self.gp_kernel = self._get_kernel()
        self.history = history
        self.objective_num = 0
        self.initial_num = initial_num
        self._get_initial()
        self.best = float("inf")
        self._init_bo(self.domain_all, np.hstack([self.knobL, self.contextL]), self.tpmL)

        self.successive_success = 0
        self.successive_fail = 0
        self.tr_radius = 0.6
        self.tr_base = self.tr_radius
        self.max_radius = max_radius
        self.adjust_flag = 0

        self.tr_bo = None
        self.line_bo = None
        self.stage = "tr"
        
        # self._initial()

    def _init_bo(self, domain , X, Y, constraints = None):
        self.bo = BayesianOptimization(f= self.func, initial_design_numdata=0,
                                       domain= domain,
                                       constraints = constraints,
                                       kernel=self.gp_kernel,
                                       X=X,
                                       Y=Y,
                                       acquisition_type='LCB',
                                       num_cores = 4,
                                       Acquisition_optimizer_type = 'CMA')

    def _get_kernel(self):
        kernel1 = GPy.kern.sde_Matern52(input_dim=self.knob_dim , variance=2., lengthscale=0.01, active_dims=[i for i in range(0, self.knob_dim)], ARD=True)
        #kernel1 = GPy.kern.RBF(input_dim=self.knob_dim , variance=1., active_dims=[i for i in range(0, self.knob_dim)], ARD=True)
        kernel2 = GPy.kern.Linear(input_dim=self.context_dim, active_dims=[i for i in range(self.knob_dim, self.knob_dim + self.context_dim)], ARD=True)
        
        return kernel1 + kernel2

    def _get_initial(self):
        #todo 
        self.knobL = np.empty(shape = (0, self.knob_dim))
        self.tpmL = np.empty(shape = (0, 1))
        self.contextL = np.empty(shape = (0, self.context_dim))

        space = Design_space(self.domain_all)
        self.initial_data = initial_design('latin', space, self.initial_num)
     
    def add_single_data(self, x, cx, y):
        if len(self.tpmL) != 0:
            if y < ( np.min(self.bo.Y.flatten()) + 0.05 * abs(np.min(self.bo.Y.flatten())) ):
                self.successive_fail = 0
                self.successive_success = self.successive_success + 1
            else :
                self.successive_success = 0
                self.successive_fail = self.successive_fail + 1

            if y > np.max(self.bo.Y.flatten()):
                self.tr_radius = self.tr_radius - 0.01

        self.knobL = np.vstack([self.knobL, x])
        self.contextL = np.vstack([self.contextL, cx])
        self.tpmL = np.vstack([self.tpmL, y])

        self.bo.X = np.vstack([self.bo.X, np.hstack([x, cx])])
        self.bo.Y = np.vstack([self.bo.Y, y])
        self.objective_num += 1

        if self.best > np.min(self.tpmL.flatten()):
            self.best = min(self.best, np.min(self.tpmL.flatten()))
            self.best_idx = np.where(self.tpmL.flatten() == self.best)[0][0]
            self.adjust_flag  = 1
            self.successive_fail = 0
            self.successive_success = 0
            print("region center move!") 

        if self.successive_fail > 4:
            self.tr_radius = self.tr_radius / 2
            self.successive_fail = 0
            self.adjust_flag  = 1
            print ("decrease tr_radius to {}".format(self.tr_radius))
        if self.successive_success > 3:
            self.tr_radius = self.tr_radius * 1.5
            self.successive_success = 0
            self.adjust_flag = 1
            print ("increase tr_radius to {}".format(self.tr_radius))

        if self.tr_radius > 0.6:
            self.tr_radius = 0.6

        if self.tr_radius < 0.5 ** 7:
            self.tr_base *= 0.75
            self.tr_radius = self.tr_base
        
        if self.adjust_flag and len(self.tpmL) >= self.initial_num:
            print("new trust region bo:")
            subdomain, X, Y = self._trust_region_subdomain(self.knobL[self.best_idx], self.tr_radius)
            self._init_bo(subdomain, X, Y)
            self.adjust_flag = 0


    def add_list_data(self, x, cx, y):

        self.knobL = np.vstack([self.knobL, x])
        self.contextL = np.vstack([self.contextL, cx])
        self.tpmL = np.vstack([self.tpmL, y])

        self.bo.X = np.vstack([self.bo.X, np.hstack([x, cx])])
        self.bo.Y = np.vstack([self.bo.Y, y])
        self.objective_num += len(self.tpmL)

        self.best = min(self.best, np.min(self.tpmL.flatten()))
        self.best_idx = np.where(self.tpmL.flatten() == self.best)[0][0]

        worse_counte = 0

        for i in range(len(self.tpmL)):
            sub = True
            for j in range(self.knob_dim):
                if self.knobL[i][j] < self.knobL[self.best_idx][j] - self.tr_radius or self.knobL[i][j] > self.knobL[self.best_idx][j] + self.tr_radius:
                    sub = False 
                    break
            if sub:
                if self.tpmL[i] > self.best * 0.95:
                    worse_counte += 1
                
        self.tr_radius *= np.power(0.5, worse_counte // 3) 

        if self.tr_radius > 0.6:
            self.tr_radius = 0.6

        if self.tr_radius < 0.01:
            self.tr_radius = 0.01

        if len(self.tpmL) >= self.initial_num:
            print("new trust region bo:")
            subdomain, X, Y = self._trust_region_subdomain(self.knobL[self.best_idx], self.tr_radius)
            self._init_bo(subdomain, X, Y)

    def _trust_region_subdomain(self ,center, radius):
        print("region center : ", center)
        print("region radius : ", radius)
        
        subdomain = []
        for i in range(self.knob_dim):
            d = {}
            d['name'] = self.domain_all[i]['name']
            d['type'] = self.domain_all[i]['type']

            l = max(self.domain_all[i]['domain'][0], center[i] - radius)
            u = min(self.domain_all[i]['domain'][1], center[i] + radius)

            d['domain'] = (l, u)
            subdomain.append(d)

        subdomain += self.domain_all[-self.context_dim:]

        subX = np.empty(shape = (0, self.knob_dim + self.context_dim))  
        subY = np.empty(shape = (0, 1))

        for i in range(len(self.tpmL)):
            sub = True
            for j in range(self.knob_dim):
                if self.knobL[i][j] < subdomain[j]['domain'][0] or self.knobL[i][j] > subdomain[j]['domain'][1]:
                    sub = False 
                    break
            if sub:
                newX = np.hstack([self.knobL[i], self.contextL[i]])
                subX = np.vstack([subX, newX])
                subY = np.vstack([subY, self.tpmL[i]])

        return subdomain, subX, subY
            
    def _trust_region_constraints(self ,center, radius, max_radius = None):
        print("region center: ", center)
        
        constraint = ''
        for i in range(len(center)):
            constraint += '(x[:,{}] - {}) ** 2'.format(i, center[i])
            if i != len(center) -1 :
                constraint += ' + '

        constraint += ' - {}'.format((radius * max_radius) ** 2)

        constraints = [
            {
                'name': 'constr_1',
                'constraint': constraint
            }
        ]
        return constraints

    def _line_subdomain(self ,center, direction):
        print("region center: ", center)
        
        return 

    def suggets(self, context=None):

        if len(self.tpmL) < self.initial_num:
            print("suggest random config!")
            return self.initial_data[self.objective_num]

        next_point = self.bo.suggest_next_locations(context=context)
        
        new_point = []
        for i in range(self.knob_dim + self.context_dim):
            new_point.append(next_point[0][i])

        return new_point
