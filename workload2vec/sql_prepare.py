# -*- coding: utf-8 -*-

from __future__ import print_function, division, unicode_literals

import argparse
import re
import pdb

"""
This script processes an input text file to produce data in binary
format to be used with the autoencoder (binary is much faster to read).
"""
keywordList = {'SELECT', 'UPDATE', 'DELECT', 'INSERT', 'REPLACE'}

pre_LSTM = True

def file_len(fname):
    with open(fname) as f:
        for i, l in enumerate(f):
            pass
    return i + 1

def transfer_sql(path):
    fileLen = file_len(path)
    print("We have {} lines to deal with".format(fileLen))
    tokenL, lineL = [], []
    paramL = []
    count = 0
    das_flag = False
    f_out = path + '.sql'
    f_out = open(f_out, 'w')
    with open(path, 'rb') as f:
        for line in f:
            line = line.decode('utf-8')
            #pdb.set_trace()
            sqlFlag = True if any(i in line for i in keywordList) else False
            paramsFlag = True if "PARAMS" in line else False
            count = count + 1
            if count%100000 == 0:
                print('finish {}%'.format(float(count / fileLen * 100)))
            if not sqlFlag and not paramsFlag:
                if not pre_LSTM:
                    f_out.write(line.strip() + '\n')
                continue
            
            if(paramsFlag):
                subFlag = "PARAMS(SEQNO, TYPE, DATA)="
                begin_ind = line.find(subFlag)
                line = line[begin_ind + len(subFlag):].strip()       
                result_list = re.findall(r"[(](.*?)[)]", line) 
                #print(result_list)
                params = []     
                for str in result_list:
                    items = str.split(',')
                    if items[1] == ' TIMESTAMP':
                        items[2] = "\'" + items[2] + "\'"
                    #print(items[2])
                    params.append(items[2])
                #print(params)          
                paramL.append(params)


            elif(sqlFlag):
                if paramL == None:
                    continue
                begin_ind = line.find(']', line.find('ip') + 1)
                begin_ind = 0 if begin_ind == -1 else begin_ind
                end_ind = line.find("exectime".upper())
                line = line[begin_ind:end_ind].strip()
                line = line.strip().strip(']').strip(':')
                for param in paramL:
                    newline = line
                    newline = newline.replace('?', 'placeholder')
                    for item in param:
                        #print(item)
                        newline = newline.replace('placeholder', item, 1)
                        
                    newline = newline + '\n'
                    newline =  newline.replace('(' , ' ( ').replace(')' , ' ) ').replace(',', ' , ').replace("'",  " ' ").replace('=', ' = ').replace('>', ' > ').replace('<', ' < ')
                    f_out.write(newline)
                    if "field8" in newline:
                        pdb.set_trace()
                paramL.clear()
            else:
                continue     

    f_out.close()



if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('input', help='Text file previously tokenized '
                                      '(by whitespace) and preprocessed')

    args = parser.parse_args()
    sql = transfer_sql(args.input)

