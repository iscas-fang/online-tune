
import re
import argparse
import numpy as np


tpcc_table = ["warehouse", "item", "stock", "district", "history", "oorder", "new_order", "order_line"]
tpch_table = [ "LINEITEM", "NATION", "ORDERS", "PART", "PARTSUPP", "REGION", "SUPPLIER"]

tpch_customer = ["C_ACCTBAL", ]

def initVec():
    plan = {
        'NSET' : {'count' : 0 , 'cost':    0, 'record':  0},
        'PRJT' : {'count' : 0 , 'cost':    0, 'record':  0},
        'SLCT' : {'count' : 0 , 'cost':    0, 'record':  0},
        'AAGR' : {'count' : 0 , 'cost':    0, 'record':  0},
        'FAGR' : {'count' : 0 , 'cost':    0, 'record':  0},
        'SAGR' : {'count' : 0 , 'cost':    0, 'record':  0},
        'HAGR' : {'count' : 0 , 'cost':    0, 'record':  0},
        'BLKUP' : {'count' : 0 , 'cost':    0, 'record':  0},
        'CSCN' : {'count' : 0 , 'cost':    0, 'record':  0},
        'SSEK' : {'count' : 0 , 'cost':    0, 'record':  0},
        'CSEK' : {'count' : 0 , 'cost':    0, 'record':  0},
        'SSCN' : {'count' : 0 , 'cost':    0, 'record':  0},
        'NEST LOOP' : {'count' : 0 , 'cost':    0, 'record':  0},
        'HASH JOIN' : {'count' : 0 , 'cost':    0, 'record':  0},
        'MERGE JOIN' : {'count' : 0 , 'cost':    0, 'record':  0},
        "INSERT" : {'count' : 0 , 'cost':    0, 'record':  0},
        "UPDATE" : {'count' : 0 , 'cost':    0, 'record':  0},
        "DELECT" : {'count' : 0 , 'cost':    0, 'record':  0},
        'DISTINCT' : {'count' : 0 , 'cost':    0, 'record':  0},
        "ACTRL" : {'count' : 0 , 'cost':    0, 'record':  0},
        'OTHER' : {'count' : 0 , 'cost':    0, 'record':  0}
    }

    return plan

def name_pasar(node_name):
    node_type1 = ['NSET2' ,'PRJT2' ,'SLCT2' ,'AAGR2' ,'FAGR2' ,'SAGR2' ,'HAGR2' ,'BLKUP2' ,'CSCN2','SSEK2','CSEK2','SSCN2']
    node_type2 = ['INSERT', 'UPDATE', 'DELECT', 'DISTINCT', 'ACTRL']

    node_name = re.sub(u"\\(.*?\\)", "", node_name).strip()
    
    if node_name in node_type1:
        return node_name[:-1]
    elif node_name in node_type2:
        return node_name
    
    if 'NEST LOOP' in node_name:
        return 'NEST LOOP'

    if 'JOIN' in node_name and 'HASH' in node_name:
        return 'HASH JOIN'

    if 'JOIN' in node_name and 'MERGE' in node_name:
        return 'MERGE JOIN'

    return 'OTHER'

def plan_pasar(plan):
    nodes = plan.strip().split('\n')
    plan_node = initVec()
    for node in nodes:
        node_name = re.findall(r'#(.*?):', node)
        node_name = name_pasar(node_name[0])
        result_list = re.findall(r": [[](.*?)[]]", node)
        summary = result_list[0].split(',')

        plan_node[node_name]['count'] += 1
        plan_node[node_name]['cost'] += int(summary[0])
        plan_node[node_name]['record']  += int(summary[1])

    plan_vec = []
    for node in plan_node.keys():
        for key in plan_node[node].keys():
            plan_vec.append(plan_node[node][key])

    return plan_vec

def check_string_in_list(str_list, my_string):
    my_string_lower = my_string.lower()  
    for element in str_list:
        if element.lower() in my_string_lower:  
            return True
    return False


def add_table(line):

    if "customer" in line:
        if check_string_in_list(tpch_customer, line):
            line = line.replace("customer", "tpch." + "customer")
        else:
            line = line.replace("customer", "tpcc." + "customer")


    for table in  tpcc_table:
        line = line.replace(table.lower(), "tpcc." + table.lower())

    for table in  tpch_table:
        line = line.replace(table.lower(), "tpch." + table.lower())

    return line

    
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('input', help='Text file previously tokenized '
                                      '(by whitespace) and preprocessed')
    parser.add_argument('user', help='User to login',
                                      default="tpcc")

    args = parser.parse_args()

    workloadVec = []

    
    try:
        # conn = 
        print('python: conn success!')

        f_out =  args.input + '.plan'

        with open(args.input, 'rb') as f:
            for line in f:
                line = line.decode('utf-8')

                res =  conn.explain(line)
                planVec = plan_pasar(res)
                workloadVec.append(planVec)
                
        result = np.array(workloadVec)
        np.save( f_out + '.npy', result)
    except (Error, Exception) as err:
        print(err)
        print(line)
        

