import os

import numpy as np
import random
import pandas as pd
import torch.nn as nn
import torch.optim
from torch.utils.data import Dataset, DataLoader
from multiprocessing import Process
from sklearn.preprocessing import StandardScaler, MinMaxScaler

"""
Hyper parameters
"""

class AutoEncoder(nn.Module):

    def __init__(self):
        super(AutoEncoder, self).__init__()

        self.encoder = nn.Sequential(
            nn.Linear(63, 3),
            nn.Tanh(),
        )

        self.decoder = nn.Sequential(
            nn.Tanh(),
            nn.Linear(3, 63),
            nn.Sigmoid(),
        )

    def forward(self, x):
        encoded_x = self.encoder(x)
        decoded_x = self.decoder(encoded_x)
        return encoded_x, decoded_x


class QueryDataset(Dataset):

    def __init__(self, dataSet):
        # self.data = pd.read_csv(path, index_col=0).to_numpy()
        
        df = pd.DataFrame(dataSet)

        standard_scaler = StandardScaler(copy=False)  # inplace scale
        standard_scaler.fit(df)                       # df is changed
        df = pd.DataFrame(standard_scaler.transform(df))

        minmax_scaler = MinMaxScaler(copy=False)
        minmax_scaler.fit(df)
        df = pd.DataFrame(minmax_scaler.transform(df)) 

        self.data = df.to_numpy()

    def __getitem__(self, index):
        return torch.from_numpy(self.data[index]).to(torch.float32)

    def __len__(self):
        return len(self.data)


class PlanAutoencoder(object):
    def __init__(self):

        self.model = AutoEncoder()

    def train(self, path, out_file,BATCH_SIZE, EPOCH, LR):
        plans_numpy = np.load(path, allow_pickle=True)
        valid_data, train_data = self._data_split(plans_numpy, ratio=0.05, shuffle=True)

        train_dataset = QueryDataset(train_data)
        train_data_loader = DataLoader(dataset=train_dataset, batch_size=BATCH_SIZE, shuffle=True, num_workers=0)

        valid_dataset = QueryDataset(valid_data)
        valid_data_loader = DataLoader(dataset=valid_dataset, batch_size=len(valid_data), shuffle=True, num_workers=0)

        model = self.model
        model.train()
        optimizer = torch.optim.Adam(model.parameters(), lr=LR)
        scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=20, gamma=0.5)
        loss_func = nn.MSELoss()

        best_loss =10000
        train_losses = []
        valid_losses = []
        for epoch in range(EPOCH):
            for step, vector in enumerate(train_data_loader):
                encoded_x, decoded_x = model(vector)
                loss = loss_func(decoded_x, vector)
                optimizer.zero_grad()
                loss.backward()
                optimizer.step()

                if step % 100 == 0:
                    with torch.no_grad():
                        for _, vec in enumerate(valid_data_loader):
                            _, decoded_x = model(vec)
                            valid_loss = loss_func(decoded_x, vec)

                            print('Epoch: {} | Step: {} | Learning rate: {:.6f} | Train loss: {:.5f} | Valid loss: {:.5f}'.format(
                                epoch, step, scheduler.get_last_lr()[0], loss.data.numpy(), valid_loss.data.numpy()))
                            if(valid_loss.data.numpy() < best_loss):
                                print("save_model!")
                                best_loss = valid_loss.data.numpy()
                                torch.save(model.state_dict(), out_file + '.pkl')

                            train_losses.append(loss.data.numpy())
                            valid_losses.append(valid_loss.data.numpy())

            scheduler.step()
        np.save("train_loss.npy", train_losses)
        np.save("test_loss.npy", valid_losses)
    
    def _data_split(self, data, ratio, shuffle=False):
        n_total = len(data)
        offset = int(n_total * ratio)
        if n_total == 0 or offset < 1:
            return [], data
        if shuffle:
            random.shuffle(data)
        valid_data = data[:offset]
        train_data = data[offset:]
        return valid_data, train_data

    def encode(self, path):
        plans_numpy = np.load(path, allow_pickle=True)
        dataset = QueryDataset(plans_numpy)
        data_loader = DataLoader(dataset=dataset, batch_size=len(plans_numpy), shuffle=False, num_workers=4)

        model = self.model

        with torch.no_grad():
            for step, vector in enumerate(data_loader):
                encoded_x, _ = model(vector)
                return encoded_x.numpy()

    def load(self, path):
        self.model.load_state_dict(torch.load(path))
        self.model.eval()


if __name__ == '__main__':
    # file 
    out = 'planModel'
    epoch = 50
    batch_size = 1024
    lr = 0.005
    encoder = PlanAutoencoder()
    encoder.train(path, out, batch_size, epoch, lr)
    
