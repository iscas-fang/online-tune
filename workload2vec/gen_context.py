import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import warnings
warnings.filterwarnings('ignore')
import tensorflow as tf
import numpy as np
import argparse

from plans2vec.autoencoder import PlanAutoencoder
from querys2vec.autoencoder import TextAutoencoder
from querys2vec.utils import load_text_data
from querys2vec.utils import WordDictionary

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('planModel', help='1')
    parser.add_argument('planInput', help='2')
    parser.add_argument('queryModel', help='3')    
    parser.add_argument('queryInput', help='4')
    # parser.add_argument('seconds', help='5')                   

    args = parser.parse_args()

    pEncoder = PlanAutoencoder()
    pEncoder.load(args.planModel)
    plansVec = pEncoder.encode(args.planInput)

    sess = tf.InteractiveSession()
    qEncoder = TextAutoencoder.load(args.queryModel, sess)

    wd = WordDictionary("train/vocabulary.txt")
    sentences, sizes = load_text_data(args.queryInput, wd)
    num_sents = 1000
    next_index = 0
    all_states = []
    while next_index < len(sentences):
        batch = sentences[next_index:next_index + num_sents]
        batch_sizes = sizes[next_index:next_index + num_sents]
        next_index += num_sents
        state = qEncoder.encode(sess, batch, batch_sizes)
        all_states.append(state)

    
    # qps = len(sentences) / int(args.seconds)
    querysVec = np.vstack(all_states)
    workloadVec = np.hstack((querysVec.mean(axis=0), plansVec.mean(axis=0)))
    np.save('workloadVec.npy', workloadVec)



    
