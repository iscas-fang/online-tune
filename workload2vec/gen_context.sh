#!/bin/sh

cd $2

python3 sql_prepare.py $1 &&
python3 querys2plans.py $1.sql $3 &&
python3 -W ignore gen_context.py planModel.pkl $1.sql.plan.npy model $1.sql