from distutils.log import Log
import glob
import json
from logging.config import dictConfig
import os
import re
import time
from collections import OrderedDict
from multiprocessing import Process
import numpy as np
import subprocess

from cluster.dbscan import Cluster
from optimizer.cgpbo import CGPBO
from optimizer.gpbo import GPBO
import logging
from logging.handlers import RotatingFileHandler
from util.knobs import knob2domain,get_context_domain,get_knob_details, get_default_knobs, initialize_knobs, get_knobs_radius

from fabric.api import cd, env, lcd, local, settings, show, task, hide
from fabric.state import output as fabric_output

from utils import (file_exists, get, get_content, load_driver_conf, parse_bool,
                   put, run, run_sql_script, sudo, FabricException)

# Loads the driver config file (defaults to driver_config.py)
dconf = load_driver_conf()  # pylint: disable=invalid-name
float_knobs = ['INDEX_SKIP_SCAN_RATE', 'HLDR_HOLD_RATE', 'DSC_RESERVE_PERCENT', 'CKPT_FLUSH_RATE','FILE_SCAN_PERCENT','UNDO_RETENTION' ]

perf = []
knobs = []
contexts = []

cluster = Cluster(dconf.KNOBS_NUM, dconf.CONTEXT_DIM)
algoL = []

# Fabric settings
fabric_output.update({
    'running': True,
    'stdout': True,
})
env.abort_exception = FabricException
env.hosts = [dconf.LOGIN]
env.password = dconf.LOGIN_PASSWORD

# Create local directories
# for _d in (dconf.LOG_DIR, dconf.WORKLOAD2VEC_PATH):
#     os.makedirs(_d, exist_ok=True)

# Configure logging
LOG = logging.getLogger(__name__)
LOG.setLevel(getattr(logging, dconf.LOG_LEVEL, logging.DEBUG))
Formatter = logging.Formatter(  # pylint: disable=invalid-name
    fmt='%(asctime)s [%(funcName)s:%(lineno)03d] %(levelname)-5s: %(message)s',
    datefmt='%m-%d-%Y %H:%M:%S')
ConsoleHandler = logging.StreamHandler()  # pylint: disable=invalid-name
ConsoleHandler.setFormatter(Formatter)
LOG.addHandler(ConsoleHandler)
FileHandler = RotatingFileHandler(  # pylint: disable=invalid-name
    dconf.DRIVER_LOG, maxBytes=50000, backupCount=2)
FileHandler.setFormatter(Formatter)
LOG.addHandler(FileHandler)

# get the db connection and cursor
# conn = 
# cursor = 

@task
def check_disk_usage():
    partition = dconf.DATABASE_DISK
    disk_use = 0
    if partition:
        cmd = "df -h {}".format(partition)
        out = run(cmd).splitlines()[1]
        m = re.search(r'\d+(?=%)', out)
        if m:
            disk_use = int(m.group(0))
        LOG.info("Current Disk Usage: %s%s", disk_use, '%')
    return disk_use


@task
def check_memory_usage():
    run('free -m -h')

@task
def change_base():
    LOG.info("Changing db base config...") 
    if dconf.DB_TYPE == 'dmdb':
        for key in dconf.BASE_DB_CONF.keys():
            
            if key in float_knobs:
                param_sql = 'ALTER SYSTEM SET \'{}\'={} MEMORY'.format(key.upper(), str(round(dconf.BASE_DB_CONF[key], 1)))
            elif key == 'COMMIT_WRITE':
                param_sql = 'ALTER SYSTEM SET \'{}\'=\'{}\' MEMORY'.format(key.upper(), 'IMMEDIATE,WAIT' if round(dconf.BASE_DB_CONF[key])== 0 else 'IMMEDIATE,NOWAIT')
            else:
                param_sql = 'ALTER SYSTEM SET \'{}\'={} MEMORY'.format(key.upper(), str(round(dconf.BASE_DB_CONF[key])))
            try:
                cursor.execute (param_sql)
            except (Error, Exception) as err:
                LOG.error(err)
            #LOG.info(param_sql)
        LOG.info("Change db config finished.")   

    return

@task
def change_conf(next_conf=None):
    next_conf = next_conf or {}
    LOG.info("Changing db config...") 
    if dconf.DB_TYPE == 'db':
        if next_conf == {}:
            LOG.info("Next configurations is null.")
        else:
            for key in next_conf.keys():
            
                if key in float_knobs:
                    param_sql = 'ALTER SYSTEM SET \'{}\'={} MEMORY'.format(key.upper(), str(round(next_conf[key], 1)))
                elif key == 'COMMIT_WRITE':
                    param_sql = 'ALTER SYSTEM SET \'{}\'=\'{}\' MEMORY'.format(key.upper(), 'IMMEDIATE,WAIT' if round(next_conf[key])== 0 else 'IMMEDIATE,NOWAIT')
                else:
                    param_sql = 'ALTER SYSTEM SET \'{}\'={} MEMORY'.format(key.upper(), str(round(next_conf[key])))
                try:
                    cursor.execute (param_sql)
                except (Error, Exception) as err:
                    LOG.error("error to run %s", param_sql)
            LOG.info("Change db config finished.")   

@task
def run_oltpbench_bg(workload):
    config_file = dconf.OLTPBENCH_CONFIG + workload + '_config.xml'

    if "tpcc" in workload:
        bench = "tpcc"
    elif "tpch" in workload:
        bench = "tpch"
    elif "ycsb" in workload:
        bench = "ycsb"
    
    if os.path.exists(config_file) is False:
        msg = 'oltpbench config {} does not exist, '.format(config_file)
        msg += 'please double check the option in driver_config.py'
        raise Exception(msg)
    
    cmd = "java -jar benchbase.jar -b {} -c {} --execute=true -s 5 > {} 2>&1 &".\
          format(bench, config_file, dconf.OLTPBENCH_LOG)
    with lcd(dconf.OLTPBENCH_HOME):  # pylint: disable=not-context-manager
        local(cmd)

@task
def run_benchmarksql_bg():
    if os.path.exists(dconf.BENCHMARKSQL_CONFIG) is False:
        msg = 'benchmarksql config {} does not exist, '.format(dconf.BENCHMARKSQL_CONFIG)
        msg += 'please double check the option in driver_config.py'
        raise Exception(msg)
    cmd = "./runBenchmark.sh {} > {} 2>&1 &".\
          format(dconf.BENCHMARKSQL_CONFIG, dconf.BENCHMARKSQL_LOG)
    with lcd(dconf.BENCHMARKSQL_HOME):  # pylint: disable=not-context-manager
        local(cmd)

@task
def run_benchmarksql(workload = ''):
    if os.path.exists(dconf.BENCHMARKSQL_CONFIG) is False:
        msg = 'benchmarksql config {} does not exist, '.format(dconf.BENCHMARKSQL_CONFIG)
        msg += 'please double check the option in driver_config.py'
        raise Exception(msg)

    cmd = "./runBenchmark.sh {}{} > {} 2>&1 &".\
          format(dconf.BENCHMARKSQL_CONFIG, workload,  dconf.BENCHMARKSQL_LOG)
    with lcd(dconf.BENCHMARKSQL_HOME):  # pylint: disable=not-context-manager
        local(cmd)

@task
def free_cache():
    if dconf.HOST_CONN not in ['docker', 'remote_docker']:
        with show('everything'), settings(warn_only=True):  # pylint: disable=not-context-manager
            res = sudo("sh -c \"echo 3 > /proc/sys/vm/drop_caches\"")
            if res.failed:
                LOG.error('%s (return code %s)', res.stderr.strip(), res.return_code)
    else:
        res = sudo("sh -c \"echo 3 > /proc/sys/vm/drop_caches\"", remote_only=True)


@task
def is_ready_db(interval_sec=10):
    
    LOG.info('database %s connecting function is not implemented, sleep %s seconds and return',
                 dconf.DB_TYPE, dconf.RESTART_SLEEP_SEC)
    time.sleep(dconf.RESTART_SLEEP_SEC)
    return

@task
def clean_logs():
    # remove oltpbench and controller log files
    local('rm -f {}'.format(dconf.OLTPBENCH_LOG))
    local('rm -f {}'.format(dconf.BENCHMARKSQL_LOG))

@task
def clean_oltpbench_results():
    # remove oltpbench result files
    local('rm -f {}/results/*'.format(dconf.OLTPBENCH_HOME))


def _ready_to_shut_down():
    ready = False
    if os.path.exists(dconf.OLTPBENCH_LOG):
       with open(dconf.OLTPBENCH_LOG, 'r') as f:
           content = f.read()
       if 'Failed' in content or 'Exception' in content:
           error_msg = content
           LOG.error('OLTPBench Failed!')
           return True, error_msg
       ready = 'com.oltpbenchmark.DBWorkload writeHistograms' in content
    return ready, None

def _finish_warmup():
    ready = False
    if os.path.exists(dconf.OLTPBENCH_LOG):
       with open(dconf.OLTPBENCH_LOG, 'r') as f:
           content = f.read()
       if 'Failed' in content or 'Exception' in content:
           error_msg = content
           LOG.error('OLTPBench Failed!')
           return True, error_msg
       ready = 'Warmup complete, starting measurements' in content
    return ready, None

# # OLTPbench summary
def get_metrics():
    summary_file = ''
    summary_path = dconf.OLTPBENCH_HOME + '/results/'

    for f_name in os.listdir(summary_path):
        if f_name.endswith('summary.json'):
            summary_file = f_name

    with open(summary_path + summary_file, 'r') as f:
        info = json.load(f)

    metrics = {}
    metrics["throughput"] = info["Throughput (requests/second)"]
    metrics["avg_latency"] = info["Latency Distribution"]["Average Latency (microseconds)"]
    return metrics

#set dameng sql log 
def set_svr_log(stat):
    mode = '0' if stat == 0 else '1'
    svr_log_sql = 'ALTER SYSTEM SET \'SVR_LOG\'={} MEMORY'.format(mode)
    try:
        cursor.execute (svr_log_sql)
    except (Error, Exception) as err:
        LOG.error(err)
    LOG.info(svr_log_sql)

@task
def clean_log_file():
    run("rm -f {}*".format(dconf.SQLLOG_FIIE))

@task
def get_context():
    workloadVec_file = "workloadVec.npy"
    if dconf.HOST_CONN != 'local':
        remote_path = os.path.join(dconf.WORKLOAD2VEC_PATH, workloadVec_file)
        local_path = os.path.join(dconf.WORKLOAD2VEC_PATH, workloadVec_file)
        get(remote_path, local_path)
    else:
        local_path = os.path.join(dconf.WORKLOAD2VEC_PATH, workloadVec_file)
    a = np.load(local_path, allow_pickle=True)
    return a

@task
def gen_context(w):
    if "tpch" in w :
        user = "tpch"
    else:
        user = "tpcc"
    run("{}/gen_context.sh {} {} {} >/dev/null 2>&1".format(dconf.WORKLOAD2VEC_PATH, dconf.SQLLOG_FIIE, dconf.WORKLOAD2VEC_PATH, user))    

def init_regions(f, domain):
    label_set = list(set(cluster.dbscan_label))
    algoL.clear()
    cluster.label_id = {}
    n_regions = len(label_set) if len(label_set) > 0 else 1

    for i in range(n_regions):
        print("init trust regions {}:".format(i))
        algorithm = CGPBO(f, domain, dconf.KNOBS_NUM, max_radius, dconf.CONTEXT_DIM)

        if len(label_set) > 0:
            X, CX , Y = cluster.clusters_to_gps_objective_one_label(label_set[i])
            algorithm.add_list_data(X, CX, Y)
            cluster.label_id[label_set[i]] = i
        else:
            cluster.label_id[-1] = 0
        algoL.append(algorithm)

def choose_region_svm(context, f,  domain_all):

        if cluster.ifReCluster():
            print("Recluster !!!!")
            cluster.append_count = 0
            cluster.dbscan_(cluster.contextL)
            init_regions(f,  domain_all)


        label = cluster.predict_cluster(context)
        cluster.current_id = cluster.label_id[label]
        print ("Choose algorithm {}".format(cluster.current_id))
        
        return cluster.current_id

def f():
    raise NotImplementedError

def gen_config(next, knob_details):
    i = 0
    config = {}
    for key in knob_details:
        if knob_details[key]['type'] == 'float':
            config[key] = round((next[i] * (knob_details[key]['max'] - knob_details[key]['min'])) + knob_details[key]['min'], 1)
        elif knob_details[key]['type'] == 'enum':
            config[key] = int((next[i] * (knob_details[key]['max'] - knob_details[key]['min'] + 1)) + knob_details[key]['min'])
            if config[key] > knob_details[key]['max']:
                config[key] = knob_details[key]['max']
        else:
            config[key] = round((next[i] * (knob_details[key]['max'] - knob_details[key]['min'])) + knob_details[key]['min'])
        i += 1
    return config

def gen_domain_context(context):
    domain_context = {}
    for i in range(dconf.CONTEXT_DIM):
        domain_context['context%s'%i] = context[i]
    return domain_context

@task
def loop(w, domain_all, knobs_detail, default = False):
    
    clean_oltpbench_results()
    clean_logs()
    clean_log_file()
    free_cache()
    default_config = get_default_knobs()
    change_conf(default_config)
    # check disk usage
    if check_disk_usage() > dconf.MAX_DISK_USAGE:
        LOG.warning('Exceeds max disk usage %s', dconf.MAX_DISK_USAGE)

    set_svr_log(1)
    # run_benchmarksql(w)
    # LOG.info('Run BenchmarkSQL : %s', w)
    # time.sleep(30)
    run_oltpbench_bg(w)
    LOG.info('Run OLTP-Bench')
    #time.sleep(5)
    ready_to_shut_down = False
    error_msg = None
    run_time = 0
    while not ready_to_shut_down:
        if run_time > dconf.RUN_TIME:
            error_msg = 'run woakload timeout!!'
            break 
        
        ready_to_shut_down, error_msg = _finish_warmup()
        time.sleep(0.5)
        run_time += 1
    set_svr_log(0)

    gen_context(w)
    context =  get_context()
    # context = np.zeros(dconf.CONTEXT_DIM)

    print(context)

    id = choose_region_svm(context, f , domain_all)

    if default:
        next_config = get_default_knobs()
        LOG.info("use default knobs!")
        next = []
        for key in next_config:
            next.append((next_config[key] - knobs_detail[key]['min']) / (knobs_detail[key]['max'] - knobs_detail[key]['min']))
        next += context.tolist()
    else:
        domain_context = gen_domain_context(context)
        next = algoL[id].suggets(context = domain_context)
        next_config = gen_config(next, knobs_detail)

    print("suggest knobs: " , next_config)

    change_conf(next_config)
    
    # stop the experiment
    ready_to_shut_down = False
    error_msg = None
    run_time = 0
    while not ready_to_shut_down:
        if run_time > dconf.RUN_TIME:
            error_msg = 'run woakload timeout!!'
            break 

        ready_to_shut_down, error_msg = _ready_to_shut_down()
        time.sleep(1)
        run_time += 1

    if error_msg:
       raise Exception('OLTP-Bench Failed: ' + error_msg)
    # if error_msg:
    #     raise Exception('BenchmarkSQL Failed: ' + error_msg)
    LOG.info('Run OLTP-Bench finished')

    metrics = get_metrics()

    label = cluster.get_label(id)

    if dconf.TUNE_TARGET == 'throughput':
        objective = - metrics["throughput"]
    else:
        objective = metrics["avg_latency"]

    cluster.add_data_to_cluster(context, objective, next[:dconf.KNOBS_NUM], label)
    algoL[id].add_single_data(next[:dconf.KNOBS_NUM], context, objective)

    perf.append(metrics)
    contexts.append(context)
    LOG.info('throughput: %f', metrics["throughput"])
    LOG.info('avg_latency: %f', metrics["avg_latency"])


@task
def run_tune(epoch=4):
    global max_radius
    global knobs_detail

    epoch = dconf.EPOCH
    iter = dconf.ITER
    workload = dconf.WORKLOAD_PATTEN

    knobs_detail =  get_knob_details(dconf.DYNAMIC_KNOBS_FILE, dconf.KNOBS_NUM)

    max_radius = get_knobs_radius(knobs_detail)
    print("max bounds radius:", max_radius)

    knob_domain = knob2domain(knobs_detail)
    context_domain = get_context_domain(dconf.CONTEXT_DIM)
    domain_all = knob_domain + context_domain

    init_regions(f, domain_all)

    count = 0
    max_count = epoch * iter * len(workload)

    change_base()

    for i in range(epoch):
        for w in workload:
            for e in range(iter):
                LOG.info('The %s-th Loop Starts / Total Loops %s', count + 1, max_count)
                if i==0 and e ==0:
                    loop(w, domain_all, knobs_detail, True)
                else:
                    loop(w, domain_all, knobs_detail)
                LOG.info('The %s-th Loop Ends / Total Loops %s', count + 1, max_count)
                count += 1

    result = np.array(perf)
    np.save('result/{}_result.npy'.format(dconf.TUNE_METHORD), result)
    contextRe = np.array(contexts)
    np.save('result/{}_context.npy'.format(dconf.TUNE_METHORD), contextRe)
    conn.close()
    LOG.info('python: conn close')